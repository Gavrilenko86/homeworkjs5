function createNewUser() {
    let firstName, lastName;
    
    do {
      firstName = prompt("Введіть ваше ім'я:");
    } while (!/^[a-zA-Z]+$/.test(firstName)); 
    
    do {
      lastName = prompt("Введіть ваше прізвище:");
    } while (!/^[a-zA-Z]+$/.test(lastName)); 
    
    const newUser = {
      firstName: firstName,
      lastName: lastName,
      getLogin: function() {
        return (this.firstName.charAt(0) + this.lastName).toLowerCase();
      }
    };
    
    return newUser;
  }
  
  const user = createNewUser();
  console.log(user.getLogin()); 